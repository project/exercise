<?php


/**
 * The exercise plan's form page
 */
function exerciseplan_form_page() {
  drupal_add_js(drupal_get_path('module', 'exerciseplan') . '/js/jquery.mousewheel.js');
  drupal_add_js(drupal_get_path('module', 'exerciseplan') . '/js/jquery.jScrollPane.min.js');
  drupal_add_js(drupal_get_path('module', 'exerciseplan') . '/js/jquery.bltcheckbox.js');
  drupal_add_js(drupal_get_path('module', 'exerciseplan') . '/js/jquery.textOverflow.js');
  drupal_add_js(
    array(
      'exerciseplan' => array(
        'url' => url('exerciseplan'),
        'skipFirstStep' => variable_get('exerciseplan_skip_first_step', 0),
        'perodicExample' => variable_get('exerciseplan_perodic_example', ''),
        'workoutExample' => variable_get('exerciseplan_workout_example', '')
      ),
      'exercise' => array(
        'url' => url('exercise')
      )
    ),
    'setting'
  );
  drupal_add_js(drupal_get_path('module', 'exerciseplan') . '/js/exerciseplan.js');
  return drupal_get_form('exerciseplan_form');
}

/**
 * The form for creating an exercise plan. This form will be made into a
 * multistep form using javascript. We're using javascript for this to improve
 * performance and scalability.
 */
function exerciseplan_form(&$form_state) {
  /* 2nd step, choose plan name and type */
  $form['plan_name'] = array(
    '#type' => 'textfield',
    '#maxlength' => 64,
  );
  $form['plan_type'] = array(
    '#type' => 'radios',
    '#options' => array(t('Workout plan - warm up plan'), t('Perodic plan - weekly schedule')),
  );

  /* 3rd step, perodic plan */
  $form['plan_objective'] = array(
    '#type' => 'textfield',
    '#title' => t('Objective'),
    '#maxlength' => 256,
    '#size' => 60.
  );
  // Week number
  $week = date('W');
  $form['plan_' . $week] = array(
    '#type' => 'textfield',
    '#default_value' => $week,
    '#maxlength' => 2,
  );

  // Add one week
  for ($i = 0; $i < 7; $i++) {
    $form['plan_' . $week . '_' . $i . '_0'] = array(
      '#type' => 'textfield',
      '#maxlength' => 30,
    );
  }

  /* 5th step, create a detailed plan for a workout session */
  $form['exercise_plan_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Topic'),
    '#maxlength' => 64,
    '#size' => 14,
  );
  $form['exercise_plan_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Date'),
    '#maxlength' => 32,
    '#size' => 8,
  );
  $form['exercise_plan_objective'] = array(
    '#type' => 'textfield',
    '#title' => t('Objective'),
    '#maxlength' => 64,
    '#size' => 48,
  );
  $form['exercises'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  $form['exercise_amount'] = array(
    '#type' => 'textarea',
    '#cols' => '20',
    '#rows' => '4',
  );
  $form['exercise_why'] = array(
    '#type' => 'textarea',
    '#cols' => '25',
    '#rows' => '4',
  );

  /* 6th step, exercise library */
  $vid = variable_get('exerciseplan_vocabulary', NULL);
  if (module_exists('i18ntaxonomy')) {
    $all_terms = exerciseplan_get_localized_tree($vid);
  }
  else {
    $all_terms = taxonomy_get_tree($vid);
  }

  $form = exerciseplan_init_filters($all_terms, $form);

  // Submit button (we need a submit button even if we're not going to use it)
  $form['submit'] = array(
    '#type' => 'submit',
  );

  // We need to know what type of plan to print
  $form['print'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  return $form;
}

function exerciseplan_init_filters($all_terms, $form, $parent = 0) {
  $terms = array();
  foreach ($all_terms as $term) {
    if ($term->parents[0] == $parent) {
      $terms[$term->tid] = $term->name;
      if (variable_get('exerciseplan_disable_filter_hierarchy', 0) != 1) {
        $form = exerciseplan_init_filters($all_terms, $form, $term->tid);
      }
    }
  }
  if (count($terms) > 0) {
    $terms[0] = t('All');
    $form['terms_' . $parent] = array(
      '#title' => ($parent == 0) ? t('Filter') : NULL,
      '#type' => 'select',
      '#options' => $terms,
      '#default_value' => 0,
      '#attributes' => ($parent == 0) ? NULL : array('class' => 'hidden'),
    );
  }
  return $form;
}

/**
 * The exercise plan's form submission handler
 */
function exerciseplan_form_submit($form, &$form_state) {
  include 'exerciseplan_print_rtf.inc';
  exerciseplan_print_rtf();
}

/**
 * Create a translated hierarchical representation of a vocabulary.
 * Uses static variables so that terms easily can be fetched again without
 * queering the database.
 *
 * @global object $language
 *   The language object for the current language
 *
 * @staticvar array $children
 *   Used to store children.
 * @staticvar array $parents
 *   Used to store parents.
 * @staticvar array $terms
 *   Used to store terms.
 *
 * @param int $vocabulary_id
 *   The primary identifier for the vocabulary.
 * @param string $language_code
 *   The code for that language that should be used. NULL is current language.
 * @param int $parent
 *   The primary identifier for the term to generate the tree under. 0 generates the tree for the entire vocabulary.
 * @param int $depth
 *   Used for internal recursion to determine the current level.
 * @param int $max_depth
 *   The number of levels to go down the tree. NULL is all levels.
 *
 * @return array
 *   The translated hierarchical representation of the vocabulary.
 */
function exerciseplan_get_localized_tree($vocabulary_id, $language_code = NULL, $parent = 0, $depth = 0, $max_depth = NULL) {
  global $language;
  static $children, $parents, $terms;

  // Use current language code by default
  if (!isset($language_code)) {
    $language_code = $language->language;
  }

  if (!isset($children[$vocabulary_id])) {
    $children[$vocabulary_id] = array();
    $result = db_query(
        "SELECT td.tid, td.*, th.parent, a.translation as name_translation, b.translation as description_translation
        FROM {term_data} td
        INNER JOIN {term_hierarchy} th ON td.tid = th.tid AND td.vid = %d
        LEFT JOIN (
          SELECT ls.source, lt.translation
          FROM {locales_source} ls
          JOIN {locales_target} lt
          ON ls.textgroup = 'taxonomy' AND lt.language = '%s' AND lt.lid = ls.lid
          ) a ON a.source = td.name
        LEFT JOIN (
          SELECT ls.source, lt.translation
          FROM {locales_source} ls
          JOIN {locales_target} lt ON ls.textgroup = 'taxonomy'
          AND lt.language = '%s' AND lt.lid = ls.lid
          ) b ON b.source = td.description
        ORDER BY td.weight, td.name",
        $vocabulary_id, $language_code, $language_code
    );

    while ($term = db_fetch_object($result)) {
      $children[$vocabulary_id][$term->parent][] = $term->tid;
      $parents[$vocabulary_id][$term->tid][] = $term->parent;
      $terms[$vocabulary_id][$term->tid] = $term;
    }
  }

  $max_depth = is_null($max_depth) ? count($children[$vocabulary_id]) : $max_depth;
  if ($children[$vocabulary_id][$parent]) {
    foreach ($children[$vocabulary_id][$parent] as $child) {
      if ($max_depth > $depth) {
        $terms[$vocabulary_id][$child]->depth = $depth;

        // Make parent into parents
        unset($terms[$vocabulary_id][$child]->parent);
        $terms[$vocabulary_id][$child]->parents = $parents[$vocabulary_id][$child];

        // If we've found a translation for name we use that.
        if (isset($terms[$vocabulary_id][$child]->name_translation)) {
          $terms[$vocabulary_id][$child]->name = $terms[$vocabulary_id][$child]->name_translation;
        }
        unset($terms[$vocabulary_id][$child]->name_translation);

        // If we've found a translation for description we use that.
        if (isset($terms[$vocabulary_id][$child]->description_translation)) {
          $terms[$vocabulary_id][$child]->description = $terms[$vocabulary_id][$child]->description_translation;
        }
        unset($terms[$vocabulary_id][$child]->description_translation);

        $tree[] = $terms[$vocabulary_id][$child];

        if ($children[$vocabulary_id][$child]) {
          $tree = array_merge($tree, cookbook_get_localized_tree($vocabulary_id, $language_code, $child, $depth + 1, $max_depth));
        }
      }
    }
  }

  return $tree ? $tree : array();
}