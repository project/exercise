
/**
 * The exercise object contains values and functions related to the
 * exercise planner.
 */
var exercise = exercise || {
  currentStep: 1,
  exercises: [],
  why: [],
  amount: [],
  names: [],
  plans: [],
  loading: '',
  save: false
};

/**
 * Add behaviors to the exercise planner
 */
$(document).ready(function() {
  // Print drupal messages
  $('.messages').each(exercise.displayMessage)

  // Skip first step
  if (Drupal.settings['exerciseplan']['skipFirstStep'] == 1) {
    exercise.currentStep++;
  }

  // Rewrite drupal form html to enable stylish radio-buttons
  $('.form-radio').parent().parent().addClass('plan-type');
  $('.form-radio').each(function(i, e) {
    $(e).parent().parent().append($(e).parent().html());
    $(e).parent().remove();
  });

  // Add stylish radio-buttons
  $('.plan-type').bltCheckbox({
    restricted: true,
    onChecked: exercise.validateStep,
    onUnchecked: exercise.validateStep
  });

  // Show dropshadow in IE
  if ($.browser.msie) {
    $('#ie-center').show();
  }

  // Make sure no one leaves by accident
  window.onbeforeunload = function () {
    if (exercise.currentStep != 7 && exercise.currentStep != 1)
      return $('#leave-message').html();
  }

  $('input').keypress(function(e) {
    if (e.which == 13)
      return false;
    else
      return true;
  });

  // Show the current step
  exercise.showStep();

  // Validate when fields change
  $('#edit-plan-name').change(exercise.validateStep);
  $('#edit-plan-objective').change(exercise.validateStep);
  $('#edit-exercise-plan-name').change(exercise.validateStep);
  $('#edit-exercise-plan-objective').change(exercise.validateStep);

  // Various mouseovers
  $('.add-exercise-field img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout).parent().click(exercise.addExerciseField);
  $('.remove-exercise-field img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout).parent().click(exercise.removeExerciseField);
  $('.big-button').parent().mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout).click(exercise.nextStep);
  $('.add-week').click(exercise.addWeek);
  $('.add-plan').click(exercise.nextStep);
  $('.pick-exercises').click(exercise.nextStep);

  // Exercise lib: load exercises on filter change
  $('#edit-terms-0').change(exercise.changeTerm);
  exercise.loading = $('#exercise-picker').html();

  // Determine which tip block to use, position and show it
  $('.tip-trigger').mouseover(exercise.showTip).mouseout(function() {
    $(this).parent().next().hide().next().hide();
  });

  // Special tip blocks inside a table
  $('#tip-trigger-table-1').mouseover(function() {
    $('#exercise-plan-tip-3').css({
      top: $(this).offset().top - $('#wrapper').offset().top - 50 - $('#body-wrapper')[0].offsetTop,
      left: $(this).offset().left - $('#wrapper').offset().left - 350
    }).show().next().css({
      top: $(this).offset().top - $('#wrapper').offset().top - 74 - $('#body-wrapper')[0].offsetTop,
      left: $(this).offset().left - $('#wrapper').offset().left - 8
    }).show();
  }).mouseout(function() {
    $('#exercise-plan-tip-3').hide().next().hide();
  });
  $('#tip-trigger-table-2').mouseover(function() {
    $('#exercise-plan-tip-4').css({
      top: $(this).offset().top - $('#wrapper').offset().top - 50 - $('#body-wrapper')[0].offsetTop,
      left: $(this).offset().left - $('#wrapper').offset().left - 350
    }).show().next().css({
      top: $(this).offset().top - $('#wrapper').offset().top - 74 - $('#body-wrapper')[0].offsetTop,
      left: $(this).offset().left - $('#wrapper').offset().left - 8
    }).show();
  }).mouseout(function() {
    $('#exercise-plan-tip-4').hide().next().hide();
  });

  // Help mouseover effect
  $('#help img').mouseover(function() {
    $('#help h3').show();
    $('#help').css('background', '#eee');
  }).mouseout(function() {
    $('#help h3').hide();
    $('#help').css('background', 'none');
  });

  // Show help
  $('#help').click(function() {
    exercise.showLightbox(exercise.loading, Drupal.settings['exerciseplan']['url']+ '/help/' + exercise.currentStep);
  });
});

/**
 * Display each drupal message
 *
 * @param int i The message's number in the line of messages
 * @param string message The message
 */
exercise.displayMessage = function(i, message) {
  // Fade in each message every 1/2 second
  setTimeout(function() {
    // Position the message
    $(message).css({
      'top': i * 60 + 20 + 'px',
      'right': '20px'
    }).fadeIn('slow');
    // Fade out after 5 seconds
    setTimeout(function() {
      $(message).fadeOut('slow');
    }, 5000);
  }, i * 500);
}

/**
 * Display the right sub filters and load the exercises when the filter terms 
 * changes.
 */
exercise.changeTerm = function() {
  $('.level2').children().children().hide();
  $('.level3').children().children().hide();
  if ($(this).children(':selected').val() != 0) {
    $('#edit-terms-' + $(this).children(':selected').val() + '-wrapper').appendTo('.level2')
    .children().val(0).show().unbind('change').change(function() {
      $('.level3').children().children().hide();
      if ($(this).children(':selected').val() != 0) {
        $('#edit-terms-' + $(this).children(':selected').val() + '-wrapper')
        .appendTo('.level3').children().show().val(0).unbind('change').change(exercise.loadExercises);
      }
      exercise.loadExercises();
    });
  }
  exercise.loadExercises();
}

/**
 * Display the tip.
 */
exercise.showTip = function() {
  var $this = $(this);
  var pos = $this.position();
  if ($this.offset('#body-wrapper').left - $('#body-wrapper').offset().left < 400 && !$this.hasClass('left')) {
    $this.addClass('left')
    var $arrow = $this.parent().next().next().children();
    var src = $arrow.attr('src');
    $arrow.attr('src', src.substring(0, src.length-4) + '_left.png');
  }
  if ($this.hasClass('left')) {
    var adjustX1 = 55;
    var adjustX2 = 31;
    var adjustY1 = -12;
    var adjustY2 = -7;
  } else {
    var adjustX1 = -344;
    var adjustX2 = -2;
    var adjustY1 = 40;
    var adjustY2 = 16;
  }
  $this.parent().next().css({
    left: $(this).position().left + adjustX1,
    top: $(this).position().top + adjustY1
  }).show().next().css({
    left: $(this).position().left + adjustX2,
    top: $(this).position().top + adjustY2
  }).show();
}

/**
 * Show the current step.
 */
exercise.showStep = function() {
  // Set progress, background and show only the right step's div
  exercise.setProgress();
  exercise.setBackground();
  for (var i = 1; i < 8; i++) {
    if (exercise.currentStep == i) {
      $('#step-' + i).show();
    }
    else {
      $('#step-' + i).hide();
    }
  }

  // Get new text for the buttons from the current step
  $('#button-next .button-right-center').html($('#step-' + exercise.currentStep + ' .button-next').html());
  $('#button-save .button-right-center').html($('#step-' + exercise.currentStep + ' .button-save').html());
  $('#button-previous .button-left-center').html($('#step-' + exercise.currentStep + ' .button-previous').html());
  $('#button-perodic-plan .button-left-center').html($('#step-' + exercise.currentStep + ' .button-perodic-plan').html());

  // Unbind mousewheel to prevent scrolling when there is no scrollbars
  $('#body-wrapper').parent().unbind('mousewheel');

  // Validate this step
  exercise.validateStep();

  // Hide and show the right buttons for this step
  if (exercise.currentStep == 1 || exercise.currentStep == 2 && Drupal.settings['exerciseplan']['skipFirstStep'] == 1) {
    $('#button-previous').hide();
    $('#welcome').show();
  } else {
    $('#button-previous').unbind('click').show().click(exercise.prevStep).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
    $('#welcome').hide();
  }
  if (exercise.currentStep < 5)
    $('#button-save').hide();
  if (exercise.currentStep > 4)
    $('#button-next').hide();

  if (exercise.currentStep == 3) {
    if (Drupal.settings['exerciseplan']['perodicExample'] != '') {
      $('#button-example').unbind('click').show().click(function() {
        exercise.showLightbox('<div id="example-plan-wrapper"><img src="' + Drupal.settings['exerciseplan']['perodicExample'] + '" alt=""/></div>');
        $('#example-plan-wrapper img').load(function() {
          $('#example-plan-wrapper').jScrollPane();
        });
      }).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
    }
    $('#button-perodic-plan').hide();
    
  }
  else if (exercise.currentStep == 5) {
    $('#button-finish').hide();
    if (Drupal.settings['exerciseplan']['workoutExample'] != '') {
      $('#button-example').unbind('click').show().click(function() {
        exercise.showLightbox('<div id="example-plan-wrapper"><img src="' + Drupal.settings['exerciseplan']['workoutExample'] + '" alt=""/></div>');
        $('#example-plan-wrapper img').load(function() {
          $('#example-plan-wrapper').jScrollPane();
        });
      }).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
    }
  } else {
    $('#button-perodic-plan').hide();
    $('#button-finish').hide();
    $('#button-example').hide();
  }

  if (exercise.currentStep == 5 && $('#edit-plan-type-1').attr('checked')) {
    $('#button-perodic-plan').unbind('click').show().click(function() {
      exercise.currentStep = 3;
      exercise.showStep();
    }).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
  } 
  else if (exercise.currentStep == 7) {
    $('#button-perodic-plan').unbind('click').show().click(function() {
      exercise.currentStep = 4;
      exercise.save = false;
      exercise.showStep();
    }).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
  }
  else {
    $('#button-perodic-plan').hide();
  }

  if (exercise.currentStep == 4 && $('#edit-plan-type-0').attr('checked'))
    $('#button-previous .button-left-center').html($('#step-2 .button-previous').html());
  
  if (exercise.currentStep > 2 && exercise.currentStep < 7) {
    $('.top-fade').show();
    $('.bottom-fade').show();
  } else {
    $('.top-fade').hide();
    $('.bottom-fade').hide();
  }


  // Let us exit at the last step without a warning
  if (exercise.currentStep == 7) {
    exercise.save = true;
  }

  // Load the exercises for step 6
  if (exercise.currentStep == 6) {
    exercise.loadExercises();
  }

  // Insert scrolling if needed
  $('#body-wrapper').jScrollPane();
};

/**
 * Show the next step.
 *
 * @return boolean false
 */
exercise.nextStep = function() {
  if (exercise.currentStep == 3 && exercise.plans.length < 1 || exercise.currentStep == 2 && $('#edit-plan-type-0').attr('checked') && exercise.plans.length > 0) {
    exercise.currentStep++;
  }
  else if (exercise.currentStep == 2 && $('#edit-plan-type-0').attr('checked') || exercise.currentStep == 4 && $(this).attr('id') == 'button-next') {
    exercise.currentStep += 2;
  }
  else if (exercise.currentStep == 5 && $(this).attr('id') == 'button-next') {
    exercise.currentStep -= 2;
  }
  exercise.currentStep++;
  exercise.showStep();
  return false;
};

/**
 * Show the previous step.
 *
 * @return boolean false
 */
exercise.prevStep = function() {
  if (exercise.currentStep == 4 && $('#edit-plan-type-0').attr('checked'))
    exercise.currentStep--;
  else if (exercise.currentStep == 7) {
    exercise.save = false;
    exercise.currentStep -= 2;
  } else if (exercise.currentStep == 5) {
    exercise.save = false;
    exercise.clearForm();
  }
  exercise.currentStep--;
  exercise.showStep();
  return false;
};

/**
 * Go to the last step
 */
exercise.finish = function() {
  exercise.currentStep = 7;
  exercise.showStep();
}

/**
 * Check if a field is valid.
 *
 * @return boolean
 */
exercise.fieldIsValid = function(id) {
  switch (id) {
    case '#edit-plan-name':
      if ($('#edit-plan-name').val() == '') {
        return false;
      }
      break;
    case '#edit-plan-type':
      if (!$('#edit-plan-type-0').attr('checked') && !$('#edit-plan-type-1').attr('checked')) {
        return false;
      }
      break;
    case '#edit-plan-objective':
      if ($('#edit-plan-objective').val() == ''){
        return false;
      }
      break;
    case '#edit-exercise-plan-objective':
    case '#edit-exercise-plan-name':
      if ($('#edit-exercise-plan-objective').val() == '' || $('#edit-exercise-plan-name').val() == '') {
        return false;
      }
      break;
    case 'exercise-plan-number':
      if ($('#exercise-plan tbody').children().length < 2) {
        return false;
      }
      break;
  }
  return true;
}

/**
 * Checks to see if a step is valid.
 *
 * @return boolean
 */
exercise.validateStep = function() {
  var errors = false;
  switch (exercise.currentStep) {
    case 2:
      if (!exercise.fieldIsValid('#edit-plan-name')) {
        errors = true;
      }
      else {
        $('#plan-name-error').hide();
      }
      if (!exercise.fieldIsValid('#edit-plan-type')) {
        errors = true;
      }
      else {
        $('#plan-type-error').hide();
      }
      break;

    case 3:
      if (!exercise.fieldIsValid('#edit-plan-objective')) {
        errors = true;
      }
      else {
        $('#plan-objective-error').hide();
      }
      break;

    case 5:
      if (!exercise.fieldIsValid('#edit-exercise-plan-name')) {
        errors = true;
      }
      else {
        $('#exercise-plan-error').hide();
      }
      if (!exercise.fieldIsValid('exercise-plan-number')) {
        errors = true;
      }
      else {
        $('#exercise-plan-number-error').hide();
      }
      break;
  }
  if (errors) {
    if (exercise.currentStep == 3)
      exercise.disableFinishButton();
    if (exercise.currentStep < 5) {
      exercise.disableNextButton();
    }
    if (exercise.currentStep > 4) {
      exercise.disableSaveButton();
    }
  } else {
    if (exercise.currentStep == 3)
      exercise.enableFinishButton();
    if (exercise.currentStep < 5) {
      exercise.enableNextButton();
    }
    if (exercise.currentStep > 4) {
      exercise.enableSaveButton();
    }
  }
  return !errors;
}

/**
 * Show errors for a step
 *
 * @return boolean false
 */
exercise.showErrors = function() {
  switch (exercise.currentStep) {
    case 2:
      if (!exercise.fieldIsValid('#edit-plan-name')) {
        $('#plan-name-error').css('top', $('#edit-plan-name').offset().top - 120).show();
      }
      if (!exercise.fieldIsValid('#edit-plan-type')) {
        $('#plan-type-error').css('top', $('#edit-plan-type-0-wrapper').offset().top - 84).show();
      }
      break;
    case 3:
      if (!exercise.fieldIsValid('#edit-plan-objective')) {
        $('#plan-objective-error').show();
        $('#body-wrapper').jScrollPane();
      }
      break;
    case 5:
      if (!exercise.fieldIsValid('#edit-exercise-plan-name')) {
        $('#exercise-plan-error').show();
        $('#body-wrapper').jScrollPane();
      }
      if (!exercise.fieldIsValid('exercise-plan-number')) {
        $('#exercise-plan-number-error').show();
        $('#body-wrapper').jScrollPane();
      }
      break;
  }
  return false;
}

/**
 * Run save function on the current step.
 *
 * @return boolean false
 */
exercise.saveStep = function() {
  switch (exercise.currentStep) {
    case 5:
      exercise.savePlan();
      break;
    case 6:
      exercise.saveExercises();
      break;
    case 7:
      // Merge the nested plans array into a single string
      var send = '';
      for (var i = 0; i < exercise.plans.length; i++) {
        send += exercise.plans[i][0] + '|' + exercise.plans[i][1] + '|' + exercise.plans[i][5] + '|';
        for (var n = 0; n < exercise.plans[i][2].length; n++)
          send += exercise.plans[i][2][n] + '#';
        send += '|';
        for (var n = 0; n < exercise.plans[i][3].length; n++)
          send += exercise.plans[i][3][n] + '#';
        send += '|';
        for (var n = 0; n < exercise.plans[i][4].length; n++)
          send += exercise.plans[i][4][n] + '#';
        send += '|';
        for (var n = 0; n < exercise.plans[i][6].length; n++)
          send += exercise.plans[i][6][n] + '#';
        send += '$';
      }
      // Put the data into the form and submit it.
      if ($('#exercise-plans-hidden').val() ==  undefined)
        $('<input type="hidden" name="exercise_plans" id="exercise-plans-hidden" value="' + send + '"/>').appendTo('#exerciseplan-form');
      else
        $('#exercise-plans-hidden').val(send);
      $('#exerciseplan-form').trigger('submit');
      break;
  }
}

/* COMMON FUNCTIONS */
/**
 * Show the right progress images.
 */
exercise.setProgress = function() {
  var reached = exercise.currentStep - 1;
  if (exercise.currentStep == 5)
    reached--;
  if (exercise.currentStep == 6)
    reached -= 2;
  for (var i = 1; i < 5; i++) {
    var $src = $('#progress-' + i).attr('src');
    var append;
    if (i > reached)
      append = 'unrechd.png';
    else
      append = 'reached.png';
    $('#progress-' + i).attr('src', $src.substring(0, $src.length-11) + append);
  }
}
/**
 * Set the correct background image for the step.
 */
exercise.setBackground = function() {
  var background = exercise.currentStep;
  if (background == 4) {
    background++;
  }
  else if (background == 6) {
    return false;
  }
  var bg = $('#wrapper').css('background-image');
  if (bg.substring(bg.length-2, bg.length-1) == '"')
    $('#wrapper').css('background-image', bg.substring(0, bg.length-18) + background + '_background.jpg")');
  else
    $('#wrapper').css('background-image', bg.substring(0, bg.length-17) + background + '_background.jpg)');
}

/**
 * Disable the next button.
 */
exercise.disableNextButton = function() {
  exercise.buttonBackgroundInactive('#button-next');
  $('#button-next').show().unbind('click').click(exercise.showErrors).unbind('mouseover').unbind('mouseout');
}

/**
 * Disable the save button.
 */
exercise.disableSaveButton = function() {
  exercise.buttonBackgroundInactive('#button-save')
  $('#button-save').show().unbind('click').click(exercise.showErrors).unbind('mouseover').unbind('mouseout');
}

/**
 * Disable the finish button.
 */
exercise.disableFinishButton = function() {
  exercise.buttonBackgroundInactive('#button-finish')
  $('#button-finish').show().unbind('click').click(exercise.showErrors).unbind('mouseover').unbind('mouseout');
}

/**
 * Enable the next button.
 */
exercise.enableNextButton = function() {
  exercise.buttonBackgroundActive('#button-next');
  $('#button-next').show().unbind('click').click(exercise.nextStep).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
}

/**
 * Enable the save button.
 */
exercise.enableSaveButton = function() {
  exercise.buttonBackgroundActive('#button-save');
  $('#button-save').show().unbind('click').click(exercise.saveStep).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
}

/**
 * Enable the finish button.
 */
exercise.enableFinishButton = function() {
  exercise.buttonBackgroundActive('#button-finish');
  $('#button-finish').show().unbind('click').click(exercise.finish).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
}

/**
 * Display the button as active.
 *
 * @param string id Button id.
 */
exercise.buttonBackgroundActive = function(id) {
  $(id).children().each(function() {
    var bg = $(this).css('background-image');
    if (bg.substring(bg.length - 15, bg.length - 6) == '_inactive')
      $(this).css('background-image', bg.substring(0, bg.length - 15) + '.png")');
    else if (bg.substring(bg.length - 14, bg.length - 5) == '_inactive')
      $(this).css('background-image', bg.substring(0, bg.length - 14) + '.png)');
  });
};

/**
 * Display the button as inactive.
 *
 * @param string id Button id.
 */
exercise.buttonBackgroundInactive = function(id) {
  $(id).children().each(function() {
    var bg = $(this).css('background-image');
    if (bg.substring(bg.length - 11, bg.length - 6) == 'hover')
      $(this).css('background-image', bg.substring(0, bg.length-12) + '_inactive.png")');
    else if (bg.substring(bg.length - 2, bg.length - 1) == '"' && bg.substring(bg.length - 14, bg.length - 6) != 'inactive')
      $(this).css('background-image', bg.substring(0, bg.length-6) + '_inactive.png")');
    else if (bg.substring(bg.length - 10, bg.length - 5) == 'hover')
      $(this).css('background-image', bg.substring(0, bg.length-11) + '_inactive.png)');
    else if (bg.substring(bg.length - 2, bg.length - 1) != '"' && bg.substring(bg.length - 13, bg.length - 5) != 'inactive')
      $(this).css('background-image', bg.substring(0, bg.length-5) + '_inactive.png)');
  });
}

/**
 * Create hover effect for the button.
 */
exercise.buttonMouseover = function() {
  $(this).children().each(function() {
    var bg = $(this).css('background-image');
    if (bg.substring(bg.length-2, bg.length-1) == '"')
      $(this).css('background-image', bg.substring(0, bg.length - 6) + '_hover.png")');
    else
      $(this).css('background-image', bg.substring(0, bg.length - 5) + '_hover.png)');
  });
}

/**
 * Create highlight effect for button.
 */
exercise.buttonMouseout = function() {
  $(this).children().each(function() {
    var bg = $(this).css('background-image');
    if (bg.substring(bg.length-11, bg.length-6) == 'hover')
      $(this).css('background-image', bg.substring(0, bg.length - 12) + '.png")');
    else if (bg.substring(bg.length-10, bg.length-5) == 'hover')
      $(this).css('background-image', bg.substring(0, bg.length - 11) + '.png)');
  });
}

/**
 * Display the image as highlighted.
 */
exercise.imageMouseover = function() {
  var src = $(this).attr('src');
  $(this).attr('src', src.substring(0, src.length - 4) + '_hover.png');
}

/**
 * Display the image as not highlighted.
 */
exercise.imageMouseout = function() {
  var src = $(this).attr('src');
  $(this).attr('src', src.substring(0, src.length - 10) + '.png');
}

/**
 * Open the lightbox and display url.
 *
 * @param string content The content to display in the lightbox.
 * @param string url The url to load and display.
 */
exercise.showLightbox = function(content, url) {
  if (content == undefined) {
    content = exercise.loading;
  }
  $('#lightbox-overlay').show()
  $('#lightbox-content').html(content).show().css('left', ($(document).width() / 2) - ($('#lightbox-content').width() / 2));
  if (url != undefined) {
    $('#lightbox-content').load(url, function() {
      $('.help-text').jScrollPane();
    });
  }
  $('#close-wrapper').show().css('left', ($(document).width() / 2) - ($('#close-wrapper').width() / 2)).children().unbind('click').click(function() {
    $('#lightbox-content').html('');
    $('#lightbox-overlay, #lightbox-content, #close-wrapper').hide();
  }).mouseover(exercise.buttonMouseover).mouseout(exercise.buttonMouseout);
}

/* STEP 3 FUNCTIONS */

/**
 * Add a new input field to the end of a day in the perodic plan.
 */
exercise.addExerciseField = function () {
  var $this = $(this);

  // Clone last field
  var newField = $this.prev().children(':last').clone();

  // Split and update the correct part of the id
  var idParts = newField.attr('id').split('-');
  idParts[4]++;

  // Rebuild ids, name and set value to blank
  newField.attr('id', idParts[0] + '-' + idParts[1] + '-' + idParts[2] + '-' + idParts[3] + '-' + idParts[4] + '-' + idParts[5])
  newField.children('input').attr('name', idParts[1] + '_' + idParts[2] + '_' + idParts[3] + '_' + idParts[4]).attr('id', 'edit-' + idParts[1] + '-' + idParts[2] + '-' + idParts[3] + '-' + idParts[4]).val('');

  $this.prev().append(newField);
  $this.next().show();
  $('#body-wrapper').jScrollPane();
  $('div:last input', $this.prev()).focus();
}

/**
 * Remove the last input field from a day in the perodic plan.
 */
exercise.removeExerciseField = function () {
  var $this = $(this)
  var $prev = $this.prev().prev();
  if ($prev.children().length > 1)
    $prev.children(':last').remove();
  if ($prev.children().length < 2)
    $this.hide();
  $('#body-wrapper').jScrollPane();
}

/**
 * Add a new week to the perodic plan.
 *
 * TODO: Make this function shorter by creating sub-functions
 */
exercise.addWeek = function () {
  // Get the last cell in the last row
  var last = $('#perodic-plan tbody').children('tr:last').children('td:last').prev().prev();
  var id = last.children().children().attr('id').split('-'); // Get and split id
  id[2]++; // Update week number

  if (id[2] > 53)
    id[2] = 1;

  // Start of row
  var content = '<tr><td class="no-border-left">';

  var week = $('#perodic-plan tbody').children('tr:last').children('td:first').clone();
  week.children().attr('id', 'edit-plan-' + id[2] + '-wrapper')
  .children().attr('name', 'plan_' + id[2])
  .attr('id', 'edit-plan-' + id[2]);

  // Field with week number
  content += week.html() + '</td>';

  // Create a cell for each day
  for (var i = 0; i < 7; i++) {
    var new_cell = last.clone(); // Clone the last cell
    // Update ids and name
    new_cell.children().children().attr('id', 'edit-' + id[1] + '-' + id[2] + '-' + i + '-' + id[4] + '-' + id[5])
    .children().attr('id', 'edit-' + id[1] + '-' + id[2] + '-' + i + '-' + id[4]).attr('name', id[1] + '_' + id[2] + '_' + i + '_' + id[4]).val('');

    // Make sure we only have one textfield for each day
    for (var j = 1; j < new_cell.children().children().length; j++) {
      new_cell.children().children().next().remove();
    }

    if (i == 6)
      new_cell.addClass('no-border-right');

    // Add html
    content += '<td class="' + new_cell.attr('class') + '">' + new_cell.html() + '</td>'; // Add to content

    new_cell.remove();
  }
  var removeWeekButton = $('#perodic-plan tbody').children('tr:last').children('td:last').clone();
  removeWeekButton.children().show().removeClass('hidden').attr('id', 'remove-week-' + id[2]);
  content += '<td class="' + removeWeekButton.attr('class') + '">' + removeWeekButton.html() + '</td></tr>';
  removeWeekButton.remove();

  // Append the new content and update week number value for the first field
  var weekNum = parseInt(week.children().children().attr('value'), 10) + 1;
  if (weekNum > 53)
    weekNum = 1;
  $(content).appendTo('#perodic-plan tbody').children().children().children().val(weekNum);

  $('#remove-week-' + id[2]).mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout).click(function() {
    $(this).parent().parent().remove();
    var pos = -$('#body-wrapper')[0].offsetTop;
    $('#body-wrapper').jScrollPane()[0].scrollTo(pos);
  });

  // Make the new add/remove buttons clickable
  $('.add-exercise-field').click(exercise.addExerciseField).children('img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
  $('.remove-exercise-field').click(exercise.removeExerciseField).children('img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);

  $('#body-wrapper').jScrollPane()[0].scrollTo('.add-week');
}

/* STEP 5 FUNCTIONS */

/**
 * Remove exercise from the workout session.
 */
exercise.removeExercise = function() {
  $(this).parent().parent().remove();
  if ($('#step-' + exercise.currentStep + ' tbody').children().length == 1)
    $('.blank').show();
  exercise.validateStep();
  var pos = -$('#body-wrapper')[0].offsetTop;
  $('#body-wrapper').jScrollPane()[0].scrollTo(pos);
}

/**
 * Move exercise up in the workout session.
 */
exercise.moveUp = function() {
  var $tr = $(this).parent().parent();
  var $prev = $tr.prev();
  if ($prev.attr('class') == 'exercise-row' || $prev.attr('class') == 'plan-row') {
    $tr.insertBefore($prev);
    $(this).trigger('mouseout').children('img').trigger('mouseout');
  }
  if ($tr.next().attr('class') == 'plan-row') {
    var id = $tr.attr('id').split('-')[2];
    var plan = exercise.plans[id];
    exercise.plans.splice(id, 1);
    exercise.plans.splice(id - 1, 0, plan);
    var row = $('#exercise-plans tbody').children('tr:first').next();
    for (var n = 0; n < $('#exercise-plans tbody').children().length - 1; n++) {
      row.attr('id', 'plan-row-' + n);
      row = row.next();
    }
  }
}

/**
 * Move exercise down in the workout session.
 */
exercise.moveDown = function() {
  var $tr = $(this).parent().parent();
  var $next = $tr.next();
  if ($next.attr('class') == 'exercise-row' || $next.attr('class') == 'plan-row') {
    $tr.insertAfter($next);
    $(this).trigger('mouseout').children('img').trigger('mouseout');
  }
  if ($tr.prev().attr('class') == 'plan-row') {
    var id = $tr.attr('id').split('-')[2];
    var plan = exercise.plans[id];
    exercise.plans.splice(id, 1);
    exercise.plans.splice(id + 1, 0, plan);
    var row = $('#exercise-plans tbody').children('tr:first').next();
    for (var n = 0; n < $('#exercise-plans tbody').children().length - 1; n++) {
      row.attr('id', 'plan-row-' + n);
      row = row.next();
    }
  }
}

/**
 * Clear out all data for the workout session.
 */
exercise.clearForm = function() {
  var row = $('#exercise-plan tbody').children('tr:first').next();
  var times = $('#exercise-plan tbody').children().length - 1;
  for (var n = 0; n < times; n++) {
    var newRow = row.next();
    row.remove();
    if ($('#step-' + exercise.currentStep + ' tbody').children().length == 1)
      $('#exercise-plan .blank').show();
    row = newRow;
  }
  $('#edit-exercise-plan-name').attr('value', '');
  $('#edit-exercise-plan-date').attr('value', '');
  $('#edit-exercise-plan-objective').attr('value', '');
}

/**
 * Save data from the workout session.
 *
 * TODO: Make this function shorter and easier to edit by splitting it up.
 * (We can for instance move some of the click functions out of this one...)
 */
exercise.savePlan = function() {
  var plan = [
  $('#edit-exercise-plan-name').attr('value'),
  $('#edit-exercise-plan-objective').attr('value'),
  [], // Exercises
  [], // Justification
  [], // Amount
  $('#edit-exercise-plan-date').attr('value'),
  [] // Custom names
  ];

  var row = $('#exercise-plan tbody').children('tr:first').next();
  var times = $('#exercise-plan tbody').children().length - 1;
  for (var n = 0; n < times; n++) {
    plan[2].push(row.attr('id'));
    plan[3].push($('#edit-exercise-why-' + row.attr('id')).val());
    plan[4].push($('#edit-exercise-amount-' + row.attr('id')).val());
    var name = row.children('.exercise-name').children('textarea').val();
    if (name == undefined)
      name = '';
    plan[6].push(name);
    newRow = row.next();
    row.remove();
    if ($('#step-' + exercise.currentStep + ' tbody').children().length == 1)
      $('#exercise-plan .blank').show();
    row = newRow;
  }
  $('#edit-exercise-plan-name, #edit-exercise-plan-date, #edit-exercise-plan-objective').attr('value', '');
  var newPlan = $('.example-plan').clone(); 
  if (exercise.save)
    var id = $(exercise.save).attr('id').split('-')[2];
  else
    var id = exercise.plans.length;
  var planName = $('<span style="cursor: pointer">' + plan[0] + '</span>').click(exercise.loadPlan);
  $(newPlan.attr('class', 'plan-row').attr('id', 'plan-row-' + id).children()[0]).append(planName);
  $(newPlan.children()[1]).html(plan[2].length);
  $($(newPlan.children()[2]).children('img')[0]).click(exercise.loadPlan).mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
  $($(newPlan.children()[2]).children('img')[1]).click(exercise.moveUp).mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
  $($(newPlan.children()[2]).children('img')[2]).click(exercise.moveDown).mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
  $(newPlan.children()[3]).children('img').click(function() {
    var id = $(this).parent().parent().attr('id').split('-')[2];
    exercise.plans.splice(id, 1)
    $(this).parent().parent().remove();
    if ($('#exercise-plans tbody').children().length == 1)
      $('#exercise-plans tr.blank').show();
    $('#body-wrapper').jScrollPane();
  }).mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);

  if (exercise.save) {
    var after = $(exercise.save).prev();
    exercise.plans.splice($(exercise.save).attr('id').split('-')[2], 1, plan);
    $(exercise.save).remove();
    exercise.save = false;
  } else {
    var after = $('#exercise-plans tbody').children('tr:last');
    exercise.plans.push(plan);
  }
  newPlan.insertAfter(after).show();
  $('#exercise-plans tr.blank').hide();

  $('#button-next').trigger('click');
}

exercise.loadPlan = function() {
  exercise.save = $(this).parent().parent();
  var id = $(this).parent().parent().attr('id').split('-')[2];
  exercise.clearForm();
  $('#edit-exercise-plan-name').attr('value', exercise.plans[id][0]);
  $('#edit-exercise-plan-date').attr('value', exercise.plans[id][5]);
  $('#edit-exercise-plan-objective').attr('value', exercise.plans[id][1]);
  exercise.exercises = exercise.plans[id][2].slice();
  exercise.why = exercise.plans[id][3].slice();
  exercise.amount = exercise.plans[id][4].slice();
  exercise.names = exercise.plans[id][6].slice();
  exercise.currentStep += 2;
  exercise.saveStep();
}

/* STEP 6 FUNCTIONS */

/**
 * Load exercises into the exercise library/picker.
 *
 * TODO: Make this function shorter by splitting it up
 * ( $('.exercise-select').click can be moved out for instance as well as the ajax recieve function...)
 */
exercise.loadExercises = function() {
  $('#exercise-picker').html(exercise.loading).jScrollPane();

  // Data to send with ajax
  var data = {}

  // Get term
  data['term'] = $('#edit-terms-' + $('#edit-terms-' + $('#edit-terms-0 :selected').val() + '-wrapper :selected').val() + '-wrapper :selected').val();
  if (data['term'] == undefined || data['term'] == '0')
    data['term'] = $('#edit-terms-' + $('#edit-terms-0 :selected').val() + '-wrapper :selected').val();
  if (data['term'] == undefined || data['term'] == '0')
    data['term'] = $('#edit-terms-0 :selected').val();
  
  // Send data and get new exercises
  $.get(Drupal.settings['exercise']['url'] + '/all', data, function(data) {
    // Set new data
    $('#exercise-picker').html(data);

    // Hover effect for details buttons
    $('.details-button').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
    // Hover effect for add buttons
    $('.add-button').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);

    // Mark all the selected in the new list of exercises
    for (var i = 0; i < exercise.exercises.length; i++) {
      var src = $('.' + exercise.exercises[i]).children().children().attr('src');
      if (src != undefined) {
        $('.' + exercise.exercises[i]).children().children().attr('src', src.substring(0, src.length-4) + '_selected.png');
      }
    }
    
    $('.exercise-select').click(function() {
      // If color red
      var src = $(this).children().attr('src');
      if (src.substring(src.length - 18, src.length) == 'selected_hover.png') {
        // Set color white
        $(this).children().attr('src', src.substring(0, src.length-18) + 'hover.png')
        // Go through list and remove exercise if found
        for (i = 0; i < exercise.exercises.length; i++)
          if (exercise.exercises[i] == $(this).parent().attr('class')) {
            exercise.exercises.splice(i,1);
            break;
          }
      }else {
        // Set color red
        $(this).children().attr('src', src.substring(0, src.length-9) + 'selected_hover.png');
        // Add exercise to list
        exercise.exercises.push($(this).parent().attr('class'));
      }
    });
    $('.details-button').unbind('click').click(function() {
      exercise.showLightbox(exercise.loading, Drupal.settings['exercise']['url'] + '/details/' + $(this).parent().attr('class'));
    });
    if (exercise.currentStep == 6) {
      $('.exercise-title').each(function() {
        $(this).textOverflow({
          "titleAttr":true,
          "wrap":"char"
        });
        var title = $(this).attr('title');
        var html = $(this).html();
        if (title.substring(0, title.length - 1) == html.substring(0, html.length - 4))
          $(this).html(html.substring(0, html.length - 4));
      });
    }
    // Init jScrollPane
    $('#exercise-picker').jScrollPane();
  });
}

/**
 * Save selected exercises from the exercise library/picker.
 *
 * TODO: Create a separate function for recieving the json.
 */
exercise.saveExercises = function() {
  /* Show step 5 when the save button is clicked in the exercise library,
     but first add all the selected exercises to the table in step 5.     */
  if (exercise.exercises.length > 0) {
    $('.loading').show();
    $('.blank').hide();

    $.getJSON(Drupal.settings['exercise']['url'] + '/json/' + exercise.exercises.join(','), function(data) {
      // Go through the list of exercises to add
      $.each(data, function(i, item) {
        // Go through the rows that's already in the table
        var row = $('#exercise-plan tbody').children('tr:first');
        for (var n = 0; n < $('#exercise-plan tbody').children().length; n++) {
          // If the new exercise has the same name as an already existing add _id
          if (row.attr('id') == item['id']) {
            // Determine if the exercise already has an id that we need to update instead
            var parts = item['id'].split('_');
            if (parts.length > 1)
              item['id'] = parts[0] + '_' + (parseInt(parts[1],10) + 1);
            else
              item['id'] = item['id'] + '_0';
            n = 0; // Reset to make sure no existing exercise has the same id
            row = $('#exercise-plan tbody').children('tr:first');
          }
          else {
            row = row.next();
          }
        }

        var newRow = $('.template-row').clone();
        // Change or update all the necessary attributes
        newRow.attr('id', item['id']);
        newRow.attr('class', 'exercise-row');
        if (item['id'].split('_')[0] == 0) {
          var name = exercise.names.shift();
          if (name == undefined) {
            name = '';
          }
          newRow.children('.exercise-name').html('<textarea rows="4" cols="12">' + name + '</textarea>');
        }
        else {
          newRow.children('.exercise-name').html(item['title']);
          exercise.names.shift();
        }
        newRow.children('.exercise-image').children('img').attr('src', item['image']);
        newRow.children('.exercise-info').children('a').click(function() {
          exercise.showLightbox(exercise.loading, Drupal.settings['exercise']['url'] + '/details/' + item['vid'])
        }).attr('class', '')
        .children('img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
        newRow.children('.exercise-remove').children('a').click(exercise.removeExercise)
        .children('img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
        newRow.children('.exercise-move').children('.exercise-up').click(exercise.moveUp)
        .children('img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
        newRow.children('.exercise-move').children('.exercise-down').click(exercise.moveDown)
        .children('img').mouseover(exercise.imageMouseover).mouseout(exercise.imageMouseout);
        var amount = exercise.amount.shift();
        if (amount == undefined)
          amount = item['amount']
        // TODO: Isn't it a better way to select the span? We can add a class to it and use $('.class-name' newRow)?
        newRow.children('.exercise-amount').children('div').children('div').children('span').children('textarea')
        .attr('name', 'exercise_amount_' + item['id']).attr('id', 'edit-exercise-amount-' + item['id']).val(amount);
        newRow.children('.exercise-why').children('div').children('div').children('span').children('textarea')
        .attr('name', 'exercise_why_' + item['id']).attr('id', 'edit-exercise-why-' + item['id']).val(exercise.why.shift());
        newRow.show();
        $('#exercise-plan').append(newRow);
        $('.loading').hide();
        $('#body-wrapper').jScrollPane()[0].scrollTo('.pick-exercises');
        exercise.validateStep();
      });
      $('.loading').hide();
    });
  }

  // Clear exercises to add
  exercise.exercises = [];

  exercise.prevStep();
}
