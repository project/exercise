<?php


/**
 * Takes a variable number of fields from $_POST and print a perodic schedule
 * over workout sessions in RTF-format.
 */
function exerciseplan_print_rtf() {
  // Get all of the exercise plans
  $plans = array();
  $plans_raw = explode('$', $_POST['exercise_plans']);
  foreach ($plans_raw as $plan_n => $plan) {
    $plan_attributes = explode('|', $plan);
    foreach ($plan_attributes as $attribute_name => $plan_attribute) {
      if ($attribute_name > 2) {
        $attributes = explode('#', $plan_attribute);
        array_pop($attributes);
      }
      else {
        $attributes = $plan_attribute;
      }
      $plans[$plan_n][$attribute_name] = $attributes;
    }
  }

  // The last element in the array is always empty, pop it off
  array_pop($plans);

  // Perodic plan
  $plan_name = $_POST['plan_name'];
  $plan_objective = $_POST['plan_objective'];

  // Convert all fields into a structured array
  foreach ($_POST as $key => $content) {
    // A key might be something like "terms_' . $term->tid . '_' . $term_child->tid"
    // or "plan_' . $week . '_' . $i . '_0"
    $keys = explode('_', $key);

    // 54 is used because there might be 53 weeks in one year. (We start counting from 1)
    if ($keys[0] == 'plan' && $keys[1] < 54 && $keys[1] > 0 && isset($keys[2]) && isset($keys[3])) {
      // Update week number
      if ($_POST['plan_' . $keys[1]] != $keys[1] &&
        $_POST['plan_' . $keys[1]] > 0 &&
        $_POST['plan_' . $keys[1]] < 54)
        $keys[1] = $_POST['plan_' . $keys[1]];

      // Only store if we have content
      if ($content != '') {
        // $weeks[week][day][session];
        $weeks[$keys[1]][$keys[2]][$keys[3]] = $content;
      }
    }
  }

  // Load RTF lib
  require 'lib/PHPRtfLite.php';

  // Register PHPRtfLite class loader
  PHPRtfLite::registerAutoloader();

  // New RTF
  $rtf = new PHPRtfLite();

  // Paper properties
  $rtf->setLandscape();
  $rtf->setPaperWidth(29);
  $rtf->setPaperHeight(21);
  $rtf->setMargins(1.5, 1.5, 1.5, 1.5);

  // Font
  $times12 = new PHPRtfLite_Font(12, 'Times new Roman');
  $times14 = new PHPRtfLite_Font(14, 'Times new Roman');
  $times16 = new PHPRtfLite_Font(16, 'Times new Roman');
  $center = new PHPRtfLite_ParFormat('center');

  // If we found any content we start filling in the rtf document
  if (is_array($weeks)) {
    // Add section(page) for the perodic plan
    $sect = $rtf->addSection();
    $sect->setBorders(new PHPRtfLite_Border_Format(0));

    // Write text
    $sect->writeText('<b>' . t('Exercise plan') . ':</b> ' . $plan_name . '<br/>', $times16, null);
    $sect->writeText('<b>' . t('Workout schedule') . '</b><br/>', $times16, null);
    $sect->writeText('<b>' . t('Objective') . ':</b><i>' . $plan_objective . '</i>', $times12, null);

    // Table
    $table = $sect->addTable();
    $table->addColumnsList(array(1.5, 2.6, 2.6, 2.6, 2.6, 2.6, 2.6, 2.6, 5));
    $table->addRows(count($weeks) + 1, 0.5);
    $table->setBordersOfCells(new PHPRtfLite_Border_Format(0.5, '#000', 'single', 0), 1, 1, count($weeks) + 1, 9);

    // Headers
    $table->writeToCell(1, 1, '<b>' . t('Week') . '</b>', $times12, $center);
    $table->writeToCell(1, 2, '<b>' . t('Monday') . '</b>', $times12, $center);
    $table->writeToCell(1, 3, '<b>' . t('Tuesday') . '</b>', $times12, $center);
    $table->writeToCell(1, 4, '<b>' . t('Wednesday') . '</b>', $times12, $center);
    $table->writeToCell(1, 5, '<b>' . t('Thursday') . '</b>', $times12, $center);
    $table->writeToCell(1, 6, '<b>' . t('Friday') . '</b>', $times12, $center);
    $table->writeToCell(1, 7, '<b>' . t('Saturday') . '</b>', $times12, $center);
    $table->writeToCell(1, 8, '<b>' . t('Sunday') . '</b>', $times12, $center);
    $table->writeToCell(1, 9, '<b>' . t('Notes') . '</b>', $times12, $center);

    // Insert data from array
    $i = 0;
    foreach ($weeks as $week => $days) {
      $table->writeToCell($i + 2, 1, $week, $times12, $center);
      for ($day = 0; $day < 7; $day++) {
        $write = '';
        $counter = 0;
        if (is_array($days[$day])) {
          foreach ($days[$day] as $session) {
            if ($counter > 0) {
              $write .= '<br/><br/>';
            }
            $write .= $session;
            $counter++;
          }
        }
        $table->writeToCell($i + 2, $day + 2, $write, $times12, null);
      }
      $i++;
    }
  }

  // Add exercise plans
  foreach ($plans as $plan) {
    // Create new section(page) for the plan
    $sect = $rtf->addSection();
    $sect->setBorders(new PHPRtfLite_Border_Format(0));

    // Write text.
    $sect->writeText('<b>' . t('Exercise plan') . ':</b> ' . $plan_name . '<br/>', $times16, null);
    $sect->writeText('<b>' . t('Workout plan') . ':</b> ' . $plan[0] . '<br/>', $times14, null);
    $sect->writeText('<b>' . t('Date') . ':</b> ' . $plan[2] . '<br/>', $times12, null);
    $sect->writeText('<b>' . t('Objective') . ':</b><i> ' . $plan[1] . '</i>', $times12, null);

    // Add table
    $table = $sect->addTable();
    $table->addColumnsList(array(4, 3, 4, 6, 6));
    $table->addRows(2, 0.5);
    $table->addRows(count($plan[3]), 2.5);
    $table->setBordersOfCells(new PHPRtfLite_Border_Format(0.5, '#000', 'single', 0), 1, 1, count($plan[3]) + 2, 5);
    $table->setVerticalAlignmentOfCells('center', 1, 1, count($plan[3]) + 2, 5);

    // First headers
    $table->writeToCell(1, 1, '<b>' . t('What') . '</b>', $times12, $center);
    $table->mergeCells(1, 1, 1, 2);
    $table->writeToCell(1, 3, '<b>' . t('How') . '</b>', $times12, $center);
    $table->writeToCell(1, 4, '<b>' . t('Why') . '</b>', $times12, $center);
    $table->writeToCell(1, 5, '<b>' . t('Notes') . '</b>', $times12, $center);

    // Second headers
    $table->writeToCell(2, 1, '<b>' . t('Exercise') . '</b>', $times12, $center);
    $table->mergeCells(2, 1, 2, 2);
    $table->writeToCell(2, 3, '<b>' . t('Training volume') . '</b>', $times12, $center);
    $table->writeToCell(2, 4, '<b>' . t('Justification') . '</b>', $times12, $center);

    // Write each exercise from row 3
    for ($i = 0; $i < count($plan[3]); $i++) {
      $node = exercise_get($plan[3][$i]);
      if ($plan[6][$i] != '')
        $node['title'] = $plan[6][$i];
      $table->writeToCell($i + 3, 1, $node['title'], $times12);
      $table->addImageToCell($i + 3, 2, exercise_get_image_url($node['image'], FALSE, TRUE), $center, 2.8, 2.1);
      $table->writeToCell($i + 3, 3, $plan[5][$i], $times12);
      $table->writeToCell($i + 3, 4, $plan[4][$i], $times12);
    }
  }

  // Send rtf document
  $rtf->sendRtf(t('exercise_plan.rtf'));
  exit;
}