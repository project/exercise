<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t('With The Exercise Planner you can plan an individual workout or a period of training.'); ?></p>
<p><b><?php print t('How it works'); ?>:</b></p>
<p><?php print t('First you choose if you want to create a schedule or a workout plan - warm up plan.'); ?></p>
<p><?php print t('In the workout schedule you plan a longer period of training. The workout schedule gives you the opportunity to plan workouts in a weekly overview. You can of course use one weekly plan and repeat it over a period of weeks. However it may be wise to add different workouts from week to week for variation.'); ?></p>
<p><?php print t('When you are satisfied with your schedule you can plan each session. You plan a session by adding exercises from a exercise library. Here you will find all the exercises you need to set up a varied and good exercise plan. You are planning one session at a time and may plan as many sessions as you want!'); ?></p>
<p><?php print t('After you have planned all the sessions in your weekly schedule, you can download the exercises plan. Afterwards the exercise plan can be opened in a text editor and you can make the changes you want or print the plan.'); ?></p>
<p><?php print t('Press "Start" to get going!'); ?>
<p><?php print t('Good luck!'); ?></p>
</div>