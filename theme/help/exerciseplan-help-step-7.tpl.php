<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t('You have now eached the end of The Exercise Planner. If you wish to make changes, you can "Previous" any time you want.'); ?></p>
<p><?php print t('To save your exercise plan, you press "Download". You will be asked where you want to save the document. Remember to choose a folder or location that you remember.'); ?></p>
<p><?php print t('The exercise plan i saved in the format RTF. This format can be opened by OpenOffice.org Writer or Microsoft Office Word. In these programs you can makes changes or print the exercise plan. It may be convenient to bring your exercise plan to training, that way you can keep an overview and take notes.'); ?></p>
<p><?php print t("Remember that you must download your exercise plan in order for it to be stored. If you navigate away from The Exercise Planner's website without having saved, all your work will be lost. You can of course use other tabs in the browser and navigate freely in these tabs, without affecting The Exercise Planner."); ?></p>
<p><?php print t('Your exercise plan is stored until you navigate away from The Exercise Planner. It may still be a good practice to save when you get to the last step. You can make a new plan if you want to make changes.'); ?></p>
<p><?php print t('Good luck with your training!'); ?></p>
</div>
