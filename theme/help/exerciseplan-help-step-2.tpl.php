<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t('In this step you enter your name and grade and select the type of plan you want to create.'); ?></p>
<p><?php print t("If you're going to hand this in to your teacher it's important to type in your and grade. This is required to continue in The Exercise Planner."); ?></p>
<p><?php print t('You must also choose if you want to plan a single workout or a longer period consisting of several workouts.'); ?></p>
<p><?php print t('By selecting "workout plan - warm up plan" you can go ahead and plan a session directly. You can of course plan multiple sessions in a row.'); ?></p>
<p><?php print t('If you choose "Perodic plan - weekly schedule", you first set up a weekly schedule before you plan the single sessions. To plan the sessions is optional. It\'s up to you to plan your sessions so that they comply with the perodic plan. If you only wish to create a perodic plan you can go on without creating any workout plans.'); ?></p>
<p><?php print t('If you should change your mind you can always go back to this step to change name or the plan type.'); ?></p>
</div>