<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t("In the exercise library you'll get an overview of all the exercises you can incorporate into you workout plans."); ?></p>
<p><?php print t("Each exercise is described with a title, illustration and two buttons. The button with the magnifying glass gives you the opportunity to view the exercise's details. In the exercise details the procedure for the exercise, among other things, is better described."); ?></p>
<p><?php print t("By pressing the button with a checkmark, you'll mark that you want to add this exercise to the workout plan. When a exercise is marked, the color of the buttons changes to green. At any time you can unmark the exercise by pressing the button once more. You can of course mark multiple exercises and add them to your workout plan at once."); ?></p>
<p><?php print t("You can filter the exercises by categories. Then you select a exercise category from the drop down list at the top. Many main categories has sub categories. By selecting these sub categories you'll limit the number of exercises that appear. This makes it easier to find the exercises that suits your needs. You can invalidate the limitation by selecting \"Show all\" from the drop down list."); ?></p>
<p><?php print t("When you have marked all the exercises you want to add to the workout plan, you'll press the button \"Insert selected exercises\". You can of course add exercises multiple times."); ?></p>
<p><?php print t('Press "Cancel" to return to the workout plan without adding exercises.'); ?></p>
</div>