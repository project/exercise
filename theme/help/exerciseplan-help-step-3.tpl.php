<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t('First insert the objective. The objective must be described before you can go on.'); ?></p>
<p><?php print t('The perodic plan is divided into weeks. Basically, the perodic plan consists of 1 week, but you may add as many weeks as you wish.'); ?></p>
<p><?php print t('The week numbers is automatically assigned by The Exercise Planner, but you can change them if you want. You describe each session with you own words, e.g. "A jog in the forrest". If you want multiple sessions on the same day, you can press the + icon in the week day field.'); ?></p>
<p><?php print t('Press "Show example plan" to get tips on how a perodic plan might look.'); ?></p>
<p><?php print t('When you are satisfied with the weekly schedule, you can go on by pressing "Done".') . '<br/>' . t('You can go back later and change the perodic plan.'); ?></p>
</div>