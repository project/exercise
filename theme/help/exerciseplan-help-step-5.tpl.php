<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t('Here you must first add a topic for the session. The topic is the name of your workout. You must also describe a objective for this session. The date field is optional.'); ?></p>
<p><?php print t('Press "Pick exercises" to open the exercise library. You can add one or multiple exercises at once. After you have added an exercise it will appear in the workout plan.'); ?></p>
<p><b><?php print t('The workout plan consist of four main fields: "what", "how", "why" and "edit".'); ?></b></p>
<p><?php print t('Under "what" you see the exercise\'s title, a illustration of the exercise and a button with a magnifying glass. By pressing the button with a magnifying glass you can view details for the exercise. In the exercise details the procedure for the exercise, among other things, is better described.'); ?>
<p><?php print t('Under "how" you can enter the training volume for the exercise. This can be e.g. "25 minutes", or "3 sets of 10 repetitions": you decide!'); ?></p>
<p><?php print t('Under "why" you justify why you picked this exercise. The justification you write in your own words.'); ?></p>
<p><?php print t('At the end you can change the exercise under "edit". You can change the order of the exercises in the workout plan by using the buttons with arrows up / down. You can remove a exercise from the workout plan by pressing the button with the cross.'); ?></p>
<p><?php print t('Press "Show example plan" for tips on how a workout session might look.'); ?></p>
<p><?php print t('Press "Save" to proceed.'); ?></p>
</div>