<?php
?>
<h4><?php print t('Help'); ?></h4>
<div class="help-text">
<p><?php print t('Here is a list of all the workout plans you have created.'); ?></p>
<p><?php print t('The list shows you the topics and number of exercises for you workout plans. You can edit the workout plans by pressing the button with the magnifying glass. You can also change the order of the sessions by use the buttons with arrows up / down. You delete a session by pressing the button with the cross.'); ?></p>
<p><?php print t('If you have created a perodic plan first, you can go back and view the perodic plan by pressing "Perodic plan - weekly schedule". It might useful to be reminded of what sessions you have to plan to cover the perodic plan.'); ?></p>
</div>
