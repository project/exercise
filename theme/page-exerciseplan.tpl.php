<?php
$path = base_path() . drupal_get_path('module', 'exerciseplan');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <!--[if IE]>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print $path ?>/css/exerciseplan-ie.css"/>
    <![endif]-->
    <?php print $scripts ?>
  </head>
  <body>
    <?php print $messages ?>
    <div id="ie-center"><div id="ie-shadow"></div></div>
    <div id="wrapper">
      <?php print $content ?>
    </div>
    <div id="lightbox-overlay"></div>
    <div id="lightbox-content"></div>
    <div id="close-wrapper">&nbsp;<a href="#" id="button-close"><span class="button-left-left"></span><span class="button-left-center"><?php print t('Close'); ?></span><span class="button-left-right"></span></a></div>
    <?php print $closure ?>
  </body>
</html>
