<?php
?>
<h4><?php print t('Example plan'); ?></h4>
<div class="form-item" id="edit-example-plan-objective-wrapper">
  <label for="edit-example-plan-objective"><?php print t('Objective'); ?>:</label>
  <input maxlength="256" name="example_plan_objective" id="edit-example-plan-objective" size="50" value="<?php print t('Get ready for this summers swimming competition.'); ?>" class="form-text" type="text" disabled="disabled">
</div>
<table id="example-plan">
  <thead>
    <tr>
      <th class="no-border-left"><?php print t('Week'); ?></th>
      <th><?php print t('Monday'); ?></th>
      <th><?php print t('Tuesday'); ?></th>
      <th><?php print t('Wednesday'); ?></th>
      <th><?php print t('Thursday'); ?></th>
      <th><?php print t('Friday'); ?></th>
      <th><?php print t('Saturday'); ?></th>
      <th class="no-border-right"><?php print t('Sunday'); ?></th>
      <td class="remove-week top"></td>
    </tr>
  </thead>
  <tfoot>
    <tr><td colspan="8" class="last"></td><td class="remove-week bottom"></td></tr>
  </tfoot>
  <tbody>
    <tr>
      <td class="no-border-left">
        <div class="form-item" id="edit-example-plan-18-wrapper"><input maxlength="2" name="example_plan_18" id="edit-example-plan-18" size="60" value="18" class="form-text" type="text" disabled="disabled"></div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-18-0-0-wrapper">
          <input maxlength="30" name="example_plan_18_0_0" id="edit-example-plan-18-0-0" size="60" value="" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-18-1-0-wrapper">
          <input maxlength="30" name="example_plan_18_1_0" id="edit-example-plan-18-1-0" size="60" value="<?php print t('Jog'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
        <div class="form-item" id="edit-example-plan-18-1-1-wrapper">
          <input maxlength="30" name="example_plan_18_1_1" id="edit-example-plan-18-1-1" size="60" value="<?php print t('Weightlifting'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-18-2-0-wrapper">
          <input maxlength="30" name="example_plan_18_2_0" id="edit-example-plan-18-2-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-18-3-0-wrapper">
          <input maxlength="30" name="example_plan_18_3_0" id="edit-example-plan-18-3-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-18-4-0-wrapper">
          <input maxlength="30" name="example_plan_18_4_0" id="edit-example-plan-18-4-0" size="60" value="<?php print t('Swimming'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-18-5-0-wrapper">
          <input maxlength="30" name="example_plan_18_5_0" id="edit-example-plan-18-5-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td class="no-border-right">
        <div class="form-item" id="edit-example-plan-18-6-0-wrapper">
          <input maxlength="30" name="example_plan_18_6_0" id="edit-example-plan-18-6-0" size="60" value="" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
    </tr>
    <tr>
      <td class="no-border-left">
        <div class="form-item" id="edit-example-plan-19-wrapper">
          <input maxlength="2" name="example_plan_19" id="edit-example-plan-19" size="60" value="19" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-19-0-0-wrapper">
          <input maxlength="30" name="example_plan_19_0_0" id="edit-example-plan-19-0-0" size="60" value="<?php print t('Conditioning'); ?>" class="form-text" type="text" disabled="disabled" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-19-1-0-wrapper">
          <input maxlength="30" name="example_plan_19_1_0" id="edit-example-plan-19-1-0" size="60" value="" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-19-2-0-wrapper">
          <input maxlength="30" name="example_plan_19_2_0" id="edit-example-plan-19-2-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-19-3-0-wrapper">
          <input maxlength="30" name="example_plan_19_3_0" id="edit-example-plan-19-3-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-19-4-0-wrapper">
          <input maxlength="30" name="example_plan_19_4_0" id="edit-example-plan-19-4-0" size="60" value="<?php print t('Swimming'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-19-5-0-wrapper">
          <input maxlength="30" name="example_plan_19_5_0" id="edit-example-plan-19-5-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td class="no-border-right">
        <div class="form-item" id="edit-example-plan-19-6-0-wrapper">
          <input maxlength="30" name="example_plan_19_6_0" id="edit-example-plan-19-6-0" size="60" value="<?php print t('Skiing'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td class="remove-week"></td>
    </tr>
    <tr>
      <td class="no-border-left">
        <div class="form-item" id="edit-example-plan-20-wrapper">
          <input maxlength="2" name="example_plan_20" id="edit-example-plan-20" size="60" value="20" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td class="">
        <div class="form-item" id="edit-example-plan-20-0-0-wrapper">
          <input maxlength="30" name="example_plan_20_0_0" id="edit-example-plan-20-0-0" size="60" value="" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-20-1-0-wrapper">
          <input maxlength="30" name="example_plan_20_1_0" id="edit-example-plan-20-1-0" size="60" value="<?php print t('Jog'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
      </td><td>
        <div class="form-item" id="edit-example-plan-20-2-0-wrapper">
          <input maxlength="30" name="example_plan_20_2_0" id="edit-example-plan-20-2-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-20-3-0-wrapper">
          <input maxlength="30" name="example_plan_20_3_0" id="edit-example-plan-20-3-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-20-4-0-wrapper">
          <input maxlength="30" name="example_plan_20_4_0" id="edit-example-plan-20-4-0" size="60" value="<?php print t('Swimming'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
        <div class="form-item" id="edit-example-plan-20-4-1-wrapper">
          <input maxlength="30" name="example_plan_20_4_1" id="edit-example-plan-20-4-1" size="60" value="<?php print t('Weightlifting'); ?>" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td>
        <div class="form-item" id="edit-example-plan-20-5-0-wrapper">
          <input maxlength="30" name="example_plan_20_5_0" id="edit-example-plan-20-5-0" size="60" value="" class="form-text short" type="text" disabled="disabled">
        </div>
      </td>
      <td class="no-border-right">
        <div class="form-item" id="edit-example-plan-20-6-0-wrapper">
          <input maxlength="30" name="example_plan_20_6_0" id="edit-example-plan-20-6-0" size="60" value="" class="form-text" type="text" disabled="disabled">
        </div>
      </td>
      <td class="remove-week"></td>
    </tr>
  </tbody>
</table>