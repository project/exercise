<?php

$path = base_path() . drupal_get_path('module', 'exerciseplan');
?>
<h4><?php print check_plain($node['title']) ?></h4>
<table id="exercise-details">
  <tr>
    <th class="no_border_left"><?php print t('Illustration'); ?></th>
    <th><?php print t('Description'); ?></th>
    <th class="no_border_right"><?php print t('Muscles'); ?></th>
  </tr>
  <tr>
    <td class="no_border_left">
      <div id="exercise-illustration">
      <?php if ($node['illustration'] == ''): ?>
        <img src="<?php print check_url(exercise_get_image_url($node['image'], TRUE)); ?>" width="300" height="240" alt=""/>
      <?php else: ?>
        <?php print exercise_filter_illustration_html(check_markup($node['illustration'], NULL, FALSE)) ?>
      <?php endif ?>
      </div>
    </td>
    <td><?php print nl2br(check_markup($node['body'], $node['format'], FALSE)) ?></td>
    <td class="no_border_right">
      <?php if (isset($node['muscles'])): foreach ($node['muscles'] as $muscle):
        print taxonomy_image_display($muscle->tid, 'class="muscle"');
      endforeach; endif ?>
      <img src="<?php print $path; ?>/graphics/muscles_outline.png" alt="" class="muscle"/>
    </td>
  </tr>
</table>