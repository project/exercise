<?php
?>
<h4><?php print t('Example plan'); ?></h4>
<div class="form-item" id="edit-example-workout-name-wrapper">
  <label for="edit-example-workout-name"><?php print t('Name'); ?>:</label>
  <input maxlength="256" name="example_workout_name" id="edit-example-workout-name" size="50" value="<?php print t('Warm up'); ?>" class="form-text" type="text" disabled="disabled">
</div>
<div class="form-item" id="edit-example-workout-objective-wrapper">
  <label for="edit-example-workout-objective"><?php print t('Objective'); ?>:</label>
  <input maxlength="256" name="example_workout_objective" id="edit-example-workout-objective" size="50" value="<?php print t('Warm up before doing heavier exercises.'); ?>" class="form-text" type="text" disabled="disabled">
</div>
<table id="example-plan">
  <thead>
    <tr class="header-1">
      <th class="no-border-left"><?php print t('What'); ?></th>
      <th><?php print t('How'); ?></th>
      <th class="no-border-right"><?php print t('Why'); ?></th>
    </tr>
    <tr class="header-2">
      <td class="no-border-left"><?php print t('Name'); ?></td>
      <td><?php print t('Amount'); ?></td>
      <td class="no-border-right"><?php print t('Justify your exercise choice'); ?></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="no-border-left"><?php print t('Jog'); ?></td>
      <td>
        <input maxlength="30" name="example_workout_amount_1" size="20" value="10 min." type="text" disabled="disabled">
      </td>
      <td class="no-border-right">
        <textarea cols="30" rows="2" name="example_workout_why_1" disabled="disabled"><?php print t('Gradually warm-up my body.'); ?>
        </textarea>
      </td>
    </tr>
    <tr>
      <td class="no-border-left"><?php print t('Run'); ?></td>
      <td>
        <input maxlength="30" name="example_workout_amount_2" size="20" value="5 min." type="text" disabled="disabled">
      </td>
      <td class="no-border-right">
        <textarea cols="30" rows="2" name="example_workout_why_2" disabled="disabled"><?php print t('Get ready for main exercises.'); ?>
        </textarea>
      </td>
    </tr>
  </tbody>
</table>