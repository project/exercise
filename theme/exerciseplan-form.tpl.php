<?php

$path = base_path() . drupal_get_path('module', 'exerciseplan');
?>
<div id="leave-message"><?php print t('If you leave all changes to your exercise plan will be lost.'); ?></div>
<div id="top-wrapper">
  <?php if (variable_get('exerciseplan_logo', '') != ''): ?>
    <div id="logo"><a href="<?php print url('<front>') ?>"><img src="<?php print variable_get('exerciseplan_logo', '') ?>" alt=""/></a></div>
  <?php endif ?>
  <h1><span id="welcome"><?php print t('Welcome to ')  ?></span><?php print t('The Exercise Planner'); ?></h1>
</div>
<div class="top-fade"><img src="<?php print $path . '/graphics/top_fade.png' ?>" alt=""/></div>
<div id="body-wrapper">
  <div id="step-1">
    <div class="photo" title="Photo: Step Anderson (flickr)"></div>
    <h2><?php print t('Create your own exercise plan!'); ?></h2>
    <p class="no-margin-bottom"><?php print t('Here you can create:'); ?></p>
    <ul>
      <li><?php print t('workout plan or warm up plan') ?></li>
      <li><?php print t('perodic plan or weekly schedule') ?></li>
    </ul>
    <p class="no-margin-top"><?php print t('You can choose exercises from a exercise library or type in your own exercises.'); ?></p>
    <p><?php print t('Before you start picking your exercises it might be wise to think about how you would like your exercise to be.'); ?></p>
    <p><?php print t('The exercise planner will guide you step by step.'); ?></p>
    <p><b><?php print t("Please note that it isn't possible to store your exercise plan online. This won't be available until fall 2011. In the meantime you'll have to store the exercise plan on your own computer, please see the last step."); ?></b></p>
    <p><?php print t('Press "Start" to get going!'); ?></p>
    <div class="hidden button-next"><?php print t('Start'); ?></div>
  </div>
  <div id="step-2">
    <div class="photo" title="<?php print t('Photo: Poiseon Bild &amp; Text (flickr)')?>"></div>
    <h2><?php print t('Put your personal touch on the exercise plan!'); ?></h2>
    <div id="plan-name"><?php print t('Type in name and grade:') . drupal_render($form['plan_name']); ?></div>
    <div id="plan-name-error" class="error"><?php print t('Remember to fill in your name and grade!'); ?></div>
    <p><?php print t('Here you can choose to plan a single workout session or a number of workouts over a periode of time.'); ?>
      <span class="tip-trigger"><?php print t('Tip'); ?></span></p>
    <div id="plan-type-tip" class="tip"><?php print t('The workout plan gives you the opportunity to pick exercises from a exercise library and put them together in a detailed workout plan.') . '<br/><br/>' . t('The perodic plan first gives you the opportunity to schedule workouts week by week.'); ?></div>
    <div id="plan-type-tip-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
    <?php print drupal_render($form['plan_type']); ?>
    <div id="plan-type-error" class="error"><?php print t('Rememer to select a plan type!'); ?></div>
    <div class="hidden button-previous"><?php print t('Previous'); ?></div>
    <div class="hidden button-next"><?php print t('Next'); ?></div>
  </div>
  <div id="step-3">
    <div class="photo" title="Photo: Jvangalen (sxc)"></div>
    <h2><?php print t('Perodic plan - weekly schedule'); ?></h2
    <p><?php print t('Set in an objective for the exercise periode.'); ?></p>
    <p><?php print t("Type in what you're going to exercise the differen week days."); ?></p>
    <p><?php print t('Press workout sessions to plan the exercise in detail.'); ?>
      <span class="tip-trigger"><?php print t('Tip'); ?></span></p>
    <div id="perodic-plan-tip" class="tip"><?php print t('You can add multiple workouts on the same day. You can variate from week to week or be consistent and plan the same workouts each week. To add a new week simply press "Add week".'); ?></div>
    <div id="perodic-plan-tip-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
    <div id="plan-objective-error" class="error"><?php print t('Remember to fill in a plan objective!'); ?></div>
    <div id="table-top">
      <?php print drupal_render($form['plan_objective']); ?>
      <div id="plan-objective-tip-trigger"><span class="tip-trigger"><?php print t('Tip'); ?></span></div>
      <div id="plan-objective-tip" class="tip"><?php print t('Objective: What you wish to achieve from this workout session.'); ?></div>
      <div id="plan-objective-tip-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
    </div>
    <table id="perodic-plan" class="advanced">
      <thead>
        <tr><th class="no-border-left"><?php print t('Week'); ?></th>
          <th><?php print t('Monday'); ?></th>
          <th><?php print t('Tuesday'); ?></th>
          <th><?php print t('Wednesday'); ?></th>
          <th><?php print t('Thursday'); ?></th>
          <th><?php print t('Friday'); ?></th>
          <th><?php print t('Saturday'); ?></th>
          <th class="no-border-right"><?php print t('Sunday'); ?></th>
          <td class="remove-week top"></td>
        </tr>
      </thead>
      <tfoot>
        <tr><td colspan="8" class="last"><a href="#" class="add-week"><?php print t('Add week'); ?></a></td><td class="remove-week bottom"></td></tr>
      </tfoot>
      <tbody>
        <tr>
          <td class="no-border-left"><?php $week = date('W');
    print drupal_render($form['plan_' . $week]); ?></td>
          <?php for ($i = 0; $i < 7; $i++) {
            if ($i == 6) {
 ?>
              <td class="no-border-right">
<?php } else { ?>
            <td><?php } ?>
            <div>
<?php print drupal_render($form['plan_' . $week . '_' . $i . '_0']); ?>
            </div>
            <a href="#" class="add-exercise-field"><img src="<?php print $path ?>/graphics/increase_button.png" alt="<?php print t('Add'); ?>" title="<?php print t('Add'); ?>"/></a>
            <a href="#" class="remove-exercise-field"><img src="<?php print $path ?>/graphics/decrease_button.png" alt="<?php print t('Remove'); ?>" title="<?php print t('Remove'); ?>"/></a>
          </td>
<?php } ?>
          <td class="remove-week"><img src="<?php print $path; ?>/graphics/remove_week_button.png" alt="Remove" class="hidden" title="<?php print t('Remove'); ?>"/></td>
        </tr>
      </tbody>
    </table>
    <div class="hidden button-previous"><?php print t('Previous'); ?></div>
    <div class="hidden button-next"><?php print t('Workout sessions'); ?></div>
  </div>
  <div id="step-4">
    <div class="photo" title="Photo: Atroszko (sxc)"></div>
    <h2><?php print t('Workout plans'); ?></h2>
    <ul>
      <li><?php print t('This is a list of all the detailed workout plans you have made.') ?></li>
      <li><?php print t('You may add as many workout plans as you wish.') ?></li>
      <li><?php print t('Click on "Add workout plan" to plan a new workout.') ?></li>
      <li><?php print t('You can change the plans later before you go to the next step and save them on your computer.') ?></li>
    </ul>
    <table id="exercise-plans" class="advanced">
      <thead>
        <tr>
          <th class="no-border-left"><?php print t('Workout plan'); ?></th>
          <th><?php print t('Exercises'); ?></th>
          <th class="no-border-right"><?php print t('Edit'); ?></th>
          <td class="remove-plan top"></td>
        </tr>
      </thead>
      <tfoot>
        <tr class="example-plan hidden">
          <td class="no-border-left"></td>
          <td></td>
          <td class="no-border-right plans-edit">
            <img src="<?php print $path; ?>/graphics/small_details_button.png" alt="<?php print t('View'); ?>" title="<?php print t('View') . '/' . t('Edit'); ?>"/>
            <img src="<?php print $path; ?>/graphics/plan_move_up_button.png" alt="<?php print t('Move up'); ?>" title="<?php print t('Move up'); ?>"/>
            <img src="<?php print $path; ?>/graphics/plan_move_down_button.png" alt="<?php print t('Move down'); ?>" title="<?php print t('Move down'); ?>"/>
          </td>
          <td class="remove-plan"><img src="<?php print $path; ?>/graphics/remove_plan_button.png" alt="<?php print t('Remove'); ?>" title="<?php print t('Remove'); ?>"/></td>
        </tr>
        <tr><td class="last" colspan="3"><a href="#" class="add-plan"><?php print t('Add workout plan'); ?></a></td><td class="remove-plan bottom"></td></tr>
      </tfoot>
      <tbody>
        <tr class="blank">
          <td colspan="3" class="no-border-left no-border-right"><span><span class="big-button"><img src="<?php print $path; ?>/graphics/pick_exercise.png" alt="+"/><span><?php print t('Click here to add a new plan'); ?></span></span></span></td>
          <td class="remove-plan"></td>
        </tr>
      </tbody>
    </table>
    <div class="hidden button-previous"><?php print t('Perodic plan'); ?></div>
    <div class="hidden button-next"><?php print t('Finish'); ?></div>
  </div>
  <div id="step-5">
    <div class="photo" title="Photo: Atroszko (sxc)"></div>
    <h2><?php print t('New workout plan'); ?></h2>
    <ul>
      <li><?php print t("Here you'll make a workout plan - warm up plan by adding exercises from the exercise library.") ?></li>
      <li><?php print t("Start by giving your plan a name(topic for the session).") ?></li>
      <li><?php print t("Put in an objective for the session.") ?></li>
      <li><?php print t('Pick exercises from the exercise library by pressing "Pick exercises". You can add multiple exercises at once.'); ?></li>
    </ul>
    <div id="exercise-plan-error" class="error"><?php print t('Remember to fill in a topic and a objective!'); ?></div>
    <div id="table-top">
      <?php print drupal_render($form['exercise_plan_name']); ?>
      <div id="exercise-plan-name-tip-trigger"><span class="tip-trigger"><?php print t('Tip'); ?></span></div>
      <div id="exercise-plan-name-tip" class="tip"><?php print t('Endurance'); ?></div>
      <div id="exercise-plan-name-tip-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
      <?php print drupal_render($form['exercise_plan_date']); ?>
      <?php print drupal_render($form['exercise_plan_objective']); ?>
      <div id="exercise-plan-objective-tip-trigger"><span class="tip-trigger"><?php print t('Tip'); ?></span></div>
      <div id="exercise-plan-objective-tip" class="tip"><?php print t('Improve aerobic endurance.'); ?></div>
      <div id="exercise-plan-objective-tip-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
    </div>
    <div id="exercise-plan-tip-3" class="tip"><?php print t('Type in factors that are important for the execution: time, warm up/main part/ending, number of repetitions, series, pauses, intensity etc.'); ?></div>
    <div id="exercise-plan-tip-3-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
    <div id="exercise-plan-tip-4" class="tip"><?php print t("Here you can justify your choice of exercises, e.g. balanse exercise to get better at skateboarding"); ?></div>
    <div id="exercise-plan-tip-4-pointer" class="tip-pointer"><img src="<?php print $path ?>/graphics/tip_pointer.png" alt=""/></div>
    <table id="exercise-plan">
      <thead>
        <tr class="header-1">
          <th colspan="3" class="no-border-left"><?php print t('What'); ?></th>
          <th><?php print t('How'); ?></th>
          <th><?php print t('Why'); ?></th>
          <th colspan="2" class="no-border-right"><?php print t('Edit'); ?></th>
        </tr>
        <tr class="header-2">
          <td colspan="3" class="no-border-left" style="min-width: 280px"><?php print t('Exercise'); ?></td>
          <td style="min-width: 190px"><?php print t('Training volume'); ?> <span id="tip-trigger-table-1"><?php print t('Tip'); ?></span></td>
          <td><?php print t('Justify your exercise choice'); ?> <span id="tip-trigger-table-2"><?php print t('Tip'); ?></span></td>
          <td colspan="2" class="no-border-right"><?php print t('Move up, down or remove'); ?></td>
        </tr>
      </thead>
      <tfoot>
        <tr class="template-row">
          <td class="exercise-name no-border-left no-border-right"></td>
          <td class="exercise-image no-border-left no-border-right"><img src="" alt="" width="100" height="75"/></td>
          <td class="exercise-info no-border-left"><a href="#" title="<?php print t('Details'); ?>"><img src="<?php print $path . '/graphics/details_button.png'; ?>" alt="Info"/></a></td>
          <td class="exercise-amount"><?php print drupal_render($form['exercise_amount']); ?></td>
          <td class="exercise-why"><?php print drupal_render($form['exercise_why']); ?></td>
          <td class="exercise-move"><a href="#" class="exercise-up" title="<?php print t('Move up'); ?>"><img src="<?php print $path . '/graphics/move_up_button.png'; ?>" alt="Up"/></a><br/>
            <a href="#" class="exercise-down" title="<?php print t('Move down'); ?>"><img src="<?php print $path . '/graphics/move_down_button.png'; ?>" alt="Down"/></a></td>
          <td class="exercise-remove no-border-right"><a href="#" class="exercise-delete" title="<?php print t('Remove'); ?>"><img src="<?php print $path . '/graphics/remove_button.png'; ?>" alt="Remove"/></a></td>
        </tr>
        <tr class="loading"><td colspan="7" class="no-border-left no-border-right"><img src="<?php print $path ?>/graphics/ajax-loader.gif" alt=""/></td></tr>
        <tr>
          <td colspan="7" class="last"><a href="#" class="pick-exercises"><?php print t('Pick exercises'); ?></a></td>
        </tr>
      </tfoot>
      <tbody>
        <tr class="blank">
          <td colspan="7" class="no-border-left no-border-right">
            <div id="exercise-plan-number-error" class="error"><?php print t('You must add at least one exercise before you can continue!'); ?></div>
            <span><span class="big-button"><img src="<?php print $path; ?>/graphics/pick_exercise.png" alt="+"/><span><?php print t('Click here to add exercises'); ?></span></span></span></td>
        </tr>
      </tbody>
    </table>
    <div class="hidden button-previous"><?php print t('Cancel'); ?></div>
    <div class="hidden button-save"><?php print t('Save'); ?></div>
    <div class="hidden button-perodic-plan"><?php print t('Perodic plan'); ?></div>
  </div>
  <div id="step-6">
    <div class="filters">
      <div class="level1 filter"><?php print drupal_render($form['terms_0']); ?></div>
      <div class="level2 filter"></div>
      <div class="level3 filter"></div>
    </div>
    <h2><?php print t('Exercise library'); ?></h2>
    <div class="top-fade"><img src="<?php print $path . '/graphics/top_fade_short.png' ?>" alt=""/></div>
    <div id="exercise-picker">
      <div class="center"><img src="<?php print $path ?>/graphics/ajax-loader.gif" alt=""/></div>
    </div>
    <div class="hidden button-previous"><?php print t('Cancel'); ?></div>
    <div class="hidden button-save"><?php print t('Insert selected exercises'); ?></div>
  </div>
  <div id="step-7">
    <div class="photo" title="Photo: prostic (sxc)"></div>
    <h2><?php print t('You have now completed your exercise plan.'); ?></h2>
    <h3><?php print t('Click the download button to save your exercise plan.'); ?></h3>
    <p><?php print t("Your exercise plan will be saved in a document. It's recommend that you use either OpenOffice.org Writer or Microsoft Office Word to open your exercise plan. It might be wize to print your exercise plan and bring it to your exercises!"); ?></p>
    <p><?php print t('Good luck with your workouts and remember to make good use of your new exercise plan!'); ?></p>
    <div class="hidden button-previous"><?php print t('Previous'); ?></div>
    <div class="hidden button-save"><?php print t('Download'); ?></div>
    <div class="hidden button-perodic-plan"><?php print t('Workout sessions'); ?></div>
  </div>
</div>
<div class="bottom-fade"><img src="<?php print $path . '/graphics/bottom_fade.png' ?>" alt=""/></div>
<div id="buttons-wrapper">
  <div id="buttons-right">&nbsp;
    <a href="#" id="button-example"><span class="button-right-left"></span><span class="button-right-center"><?php print t('Example plan'); ?></span><span class="button-right-right"></span></a>
    <a href="#" id="button-save"><span class="button-right-left"></span><span class="button-right-center"><?php print t('Save'); ?></span><span class="button-right-right"></span></a>
    <a href="#" id="button-next"><span class="button-right-left"></span><span class="button-right-center"><?php print t('Next'); ?></span><span class="button-right-right"></span></a>
    <a href="#" id="button-finish"><span class="button-right-left"></span><span class="button-right-center"><?php print t('Finish perodic plan'); ?></span><span class="button-right-right"></span></a>
  </div>
  <div>&nbsp;
    <a href="#" id="button-previous"><span class="button-left-left"></span><span class="button-left-center"><?php print t('Previous'); ?></span><span class="button-left-right"></span></a>
    <a href="#" id="button-perodic-plan"><span class="button-left-left"></span><span class="button-left-center"><?php print t('Perodic plan'); ?></span><span class="button-left-right"></span></a>
  </div>
</div>
<div id="bottom-wrapper">
  <div id="help"><h3><?php print t('Click for help'); ?></h3><a href="#"><img src="<?php print $path . '/graphics/help_button.png'; ?>" alt="?"/></a></div>
  <div id="progress"><p><?php print t('Progress') . ':'; ?></p><img src="<?php print $path ?>/graphics/progress_unrechd.png" alt="" title="<?php print t('Choose plan type'); ?>" id="progress-1"/><img src="<?php print $path ?>/graphics/progress_unrechd.png" alt="" title="<?php print t('Workout schedule'); ?>" id="progress-2"/><img src="<?php print $path ?>/graphics/progress_unrechd.png" alt="" title="<?php print t('Workout plans'); ?>" id="progress-3"/><img src="<?php print $path ?>/graphics/progress_unrechd.png" alt="" title="<?php print t('Download'); ?>" id="progress-4"/></div>
</div>
<?php print drupal_render($form); ?>