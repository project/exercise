<?php
$path = base_path() . drupal_get_path('module', 'exerciseplan');

if (is_array($nodes)):
  foreach ($nodes as $node):?>
    <table class="exercise">
      <tr>
        <th colspan="2"><div class="exercise-title"><?php print check_plain($node['title']); ?></div></th>
      </tr>
      <tr>
        <td colspan="2"><?php print theme_exercise_image($node['image']); ?></td>
      </tr>
      <tr>
        <td><a href="#" class="<?php print $node['vid']; ?>" title="<?php print t('Click to view details for this exercise.') ?>"><img src="<?php print $path; ?>/graphics/small_details_button.png" alt="Info" class="details-button" /></a></td>
        <td class="<?php print $node['vid']; ?>"><a href="#" class="exercise-select" title="<?php print t('Click to add this exercise to your plan.') ?>"><img src="<?php print $path; ?>/graphics/select_button.png" alt="Select" class="add-button" /></a></td>
      </tr>
    </table>
  <?php endforeach ?>
<?php endif ?>
<div id="exercise-dummy"></div>